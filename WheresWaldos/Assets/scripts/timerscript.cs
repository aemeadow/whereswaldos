﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class timerscript : MonoBehaviour
{
    public float StartTime = 60f;

    public float timePerFoundTwin = 10f;

    private float timeRemaining;
    public TextMeshProUGUI TimerText;

    public GameObject _endscreen;
    

    void Start()
    {

        timeRemaining = StartTime;
        _endscreen.SetActive(false);

        
    }

    void Update()
    {
        if (timeRemaining <= 0)
        {
            Debug.Log("Time Ran Out!");

            _endscreen.SetActive(true);

            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            //Do game over stuff

                //Like a button that appears that when you click on, calls a function with SceneManager.LoadScene("SampleScene"); (which will reload the scene, basically a restart)

        }

        //Make timers with Time.deltaTime (Since update is every frame, and deltaTime is the time since the last frame, it will always* work)
        timeRemaining -= Time.deltaTime;

        TimerText.text = timeRemaining.ToString("F2") + " Seconds remaining!";

        
        //For extra pizzazz, if you want, add an effect of making the timer PULSE bigger on the 10s, so it's calling out the time more. (10s being 10 seconds, 20 seconds, etc.)



        //*As long as you're not changing the timeScale anywhere! Then you should put your timer in FixedUpdate or something.

    }

  

    //Called by the movement controller when we find twins to add more time.
    public void AddTime()
    {
        timeRemaining += timePerFoundTwin;
    }



    //Don't need these
    /*
    void decreaseTimeRemaining()
    {
        timeRemaining--;
    }

    //may not be needed, left it in there
    void timeElapsed()
    { }
    */
}
