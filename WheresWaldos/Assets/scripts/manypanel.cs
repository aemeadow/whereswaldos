﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class manypanel : MonoBehaviour
{
    // Drag & Drop your gameObjects here
    public GameObject[] GameObjectsList;
    private int shownGameObjectIndex = -1;
    SpriteRenderer m_SpriteRenderer;
    public static int ColorCountPanel;
    private static int TwinCount;
    public AudioClip bell;
    AudioSource audioSource;
    public TextMeshProUGUI countText;
    public float timeIncrease = 2;
    public bool timeElapsed = false;

    public int items;
    public int itemsRemaining;
    


    private void Start()

    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        for (int i = 0; i < GameObjectsList.Length; ++i)
            GameObjectsList[i].SetActive(false);
        SelectNextGameObject();
        SetCountText();

        //Gather how many items are remaining
        GameObject[] items;
        items = GameObject.FindGameObjectsWithTag("items");
       // itemsRemaining = items.length;

        //The timer, as a child of this gameobject, receive this and start the countdown using the timeRemaining variable
       // BroadcastMessage("Start Timer", timeRemaining);

    }


    void Update()
    {

        if (itemsRemaining == 0)
        {
            //You win!
        }

        if (timeElapsed)
        {
            //You lose!
        }
    }

    void timeHasElapsed()
    {
        timeElapsed = true;
    }

    //If the Game Controller receives this signal from a destroyed item,
    //  it sends a message to the time object to increase the time left
    void itemDestroyed()
    {
        increaseTime();

    }

    void increaseTime()
    {
       // broadcastMessage("timeIncrease", timeIncrease);
    }



    


    // Add this function as callback for your button's onClick event
    public void SelectNextGameObject()
    {
        int index = shownGameObjectIndex >= GameObjectsList.Length - 1 ? -1 : shownGameObjectIndex;
        SelectGameObject(index + 1);
    }
    public void SelectPreviousGameObject()
    {
        int index = shownGameObjectIndex <= 0 ? GameObjectsList.Length : shownGameObjectIndex;
        SelectGameObject(index - 1);
    }
    public void SelectGameObject(int index)
    {
        if (shownGameObjectIndex >= 0)
            GameObjectsList[shownGameObjectIndex].SetActive(false);
        shownGameObjectIndex = index;
        GameObjectsList[shownGameObjectIndex].SetActive(true);
    }


    private void OnTriggerEnter(Collider other)
    {
        //all these ifs check for fish catches
        
        if (other.CompareTag("npc1"))
        {
           // SelectGameObject(0);
            TwinCount += 1;
            //audioSource.PlayOneShot(bell, 0.4F);
            SetCountText();
            Debug.Log("enter");

        }


    }

    
        
    

    void SetCountText()
    {
        countText.text = "Twins Found: " + TwinCount.ToString();
        Debug.Log("points");

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (true)
        {
            if (ColorCountPanel <= 3)
            {


                //if (col.gameObject.tag == "red crayon")
                //{
                    //Debug.Log("redyes");
                    // m_SpriteRenderer.color = Color.red;
                    //Image img = GameObject.Find("Text1").GetComponent<Image>();
                    // img.color = new Color(1, 0, 0, 1);
                   // m_SpriteRenderer.color = new Color(1, 0, 0, 1);
                    //SelectNextGameObject();
                    //ColorCountPanel++;
                //}
                


            }
        }
    }
}
