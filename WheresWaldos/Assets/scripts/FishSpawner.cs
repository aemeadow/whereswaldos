﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    //public variables
    public int fishSpawned;
    public float spawnTime;

    public int AmountOfPairsToSpawn = 50;

    //private variables
    private Vector3 Min;
    private Vector3 Max;
    private float _xAxis;
    private float _yAxis;
    private float _zAxis; 

    //fish variables
    public GameObject personPrefab;


    //Stuff I added
    private List<string> names = new List<string>();

    //THIS runs when the scene starts
    private void Start()
    {
        //spawns a new fish every few seconds
        //InvokeRepeating("SpawnFish", spawnTime, spawnTime);

        //sets area for fish
        SetRanges();

        while (fishSpawned < AmountOfPairsToSpawn)
        {
            //Really spawn people but you know
            SpawnFish();
        }

        
    }

    //runs when this object is created/awakend. Which is before Start
    private void Awake()
    {
        ImportNames();
    }

    //runs every frame
    private void FixedUpdate() //<---- Also doesn't need to happen every frame, so this is now FixedUpdate
    {

        //We dont want more people to appear in this instance, so this is out
        /*
        //if not enough fish, spawn more fish
        if (fishSpawned <= 7)
        {
            //Spawn another fish
            SpawnFish();
        }
        */

        //randomizes every frame the axis points for possible spawning
        //=============== No, don't do that, big waste of resources to do math every fram for no reason. ============
        /*_xAxis = Random.Range(Min.x, Max.x);
        _yAxis = Random.Range(Min.y, Max.y);
        _zAxis = Random.Range(Min.z, Max.z);
        _randPosition = new Vector3(_xAxis, _yAxis, _zAxis);
        */
    }

    //this is where you put the range for the spawn area
    private void SetRanges()
    {
        Min = new Vector3(-160, 69.85f, 5); 
        Max = new Vector3(25, 69.85f, 17); 
    }

    //spawns random fish from list at random area
    private void SpawnFish()
    {
        

        //spawns the fish
        GameObject spawnedPerson = Instantiate(personPrefab, CreateRandomPosition(), Quaternion.identity);

        fishSpawned++;

        spawnedPerson.AddComponent<PairInformation>();

        //spawn a second one


        //Chose a random name and assign it to both.
        string randomName = null;
        int RandomNum = Random.Range(0, names.Count);
        Random rand = new Random();
        randomName = names[RandomNum];
        if (randomName != null)
        {
            Debug.Log(names.Count);
            //Remove the chosen name from the list of possible names
            names.Remove(randomName);

            //The aissigning
            Debug.Log("Assigned " + randomName + " to them");
            spawnedPerson.gameObject.GetComponent<PairInformation>().name = randomName;
            //This used to happen on awake, but that was bad because when I copied them right below, it'd give the copy different clothes.
            spawnedPerson.GetComponent<clothing>().GiveClothing();
            //copy
            GameObject tTwo = Instantiate(spawnedPerson, CreateRandomPosition(), Quaternion.identity);
            //but same name
            tTwo.gameObject.GetComponent<PairInformation>().name = randomName;
        }
        else
        {
            Debug.LogError("Ran out of Names! Checking the count: " + names.Count);
        }
    }


    private Vector3 CreateRandomPosition()
    {
        _xAxis = Random.Range(Min.x, Max.x);
        _yAxis = Random.Range(Min.y, Max.y);
        _zAxis = Random.Range(Min.z, Max.z);
        return new Vector3(_xAxis, _yAxis, _zAxis);

    }


    //Import the names from a simple comma seperated .txt file
    private void ImportNames()
    {
        //Make the TextAssest
        TextAsset allNames = Resources.Load<TextAsset>("Name List");
        //Iterate through
        string[] inputNames = allNames.text.Split(new char[] { ',' });
        //Make sure they're getting in correctly
        for (int x = 0; x < inputNames.Length; x++)
        {
            names.Add(inputNames[x]);
            //Debug.Log(inputNames[x]);
        }
    }
}
