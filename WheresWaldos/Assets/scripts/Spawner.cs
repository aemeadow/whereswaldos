﻿using UnityEngine;
using System.Collections.Generic;

namespace UnityMovementAI
{
    public class Spawner : MonoBehaviour
    {
        public GameObject obj;
        public Vector2 objectSizeRange = new Vector2(1, 2);

        public int numberOfObjects = 10;
        public bool randomizeOrientation = false;

        public float boundaryPadding = 1f;
        public float spaceBetweenObjects = 1f;

        public List<GameObject> thingsToAvoid = new List<GameObject>();

        Vector3 bottomLeft;
        Vector3 widthHeight;

        //Stuff I added
        private Dictionary<int, string> names = new Dictionary<int, string>();

        void Start()
        {
            //Get our names
            ImportNames();

            /* Find the size of the map */
            float distAway = Camera.main.WorldToViewportPoint(Vector3.zero).z;

            bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distAway));
            Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, distAway));
            widthHeight = topRight - bottomLeft;

            /* Create the create the objects */
            for (int i = 0; i < numberOfObjects; i++)
            {
                /* Try to place the objects multiple times before giving up */
                for (int j = 0; j < 10; j++)
                {
                    if (TryToCreateObject())
                    {
                        break;
                    }
                }
            }

        }

        bool TryToCreateObject()
        {
            float size = Random.Range(objectSizeRange.x, objectSizeRange.y);
            float halfSize = size / 2f;

            Vector3 spawnPos = GetRandomPosition(halfSize);


            if (CanPlaceObject(halfSize, spawnPos))
            {
                GameObject t = Instantiate(obj, spawnPos, Quaternion.identity);
                t.AddComponent<PairInformation>();

                //spawn a second one


                //Chose a random name and assign it to both.
                string randomName = null;
                int RandomNum = Random.Range(0, names.Count);
                if (names.TryGetValue(RandomNum, out string choice))
                {
                    Debug.Log(names.Count);
                    randomName = choice;
                    //Remove the chosen name from the list of possible names
                    names.Remove(RandomNum);
                    //The aissigning
                    t.gameObject.GetComponent<PairInformation>().name = randomName;

                    GameObject tTwo = Instantiate(t, GetRandomPosition(halfSize), Quaternion.identity);
                }
                else
                {
                    Debug.LogError("Ran out of Names!");
                }


                t.transform.localScale = new Vector3(size, obj.transform.localScale.y, size);


                if (randomizeOrientation)
                {
                    Vector3 euler = transform.eulerAngles;
                    euler.y = Random.Range(0f, 360f);

                    transform.eulerAngles = euler;
                }

                return true;
            }

            return false;
        }

        bool CanPlaceObject(float halfSize, Vector3 pos)
        {
            /* Make sure it does not overlap with any thing to avoid */
            foreach(GameObject building in thingsToAvoid)
            {
                if(building.GetComponentInChildren<MeshCollider>().bounds.Contains(pos)) {
                    return false;
                }
            }

            /*
            //Make sure it does not overlap with any existing object
            foreach (MovementAIRigidbody o in objs)
            {
                float dist = Vector3.Distance(o.Position, pos);

                if (dist < o.Radius + spaceBetweenObjects + halfSize)
                {
                    return false;
                }
            }
            */

            return true;
        }

        //Import the names from a simple comma seperated .txt file
        private void ImportNames()
        {
            //Make the TextAssest
            TextAsset allNames = Resources.Load<TextAsset>("Name List");
            //Iterate through
            string[] inputNames = allNames.text.Split(new char[] { ',' });
            //Make sure they're getting in correctly
            for(int x = 0; x < inputNames.Length; x++)
            {
                names.Add(x, inputNames[x]);
                //Debug.Log(inputNames[x]);
            }
        }


        private Vector3 GetRandomPosition(float halfSize)
        {

            Vector3 pos = new Vector3();
            pos.x = bottomLeft.x + Random.Range(boundaryPadding + halfSize, widthHeight.x - boundaryPadding - halfSize);

            //Where the "floor" is
            pos.y = 69.77f;

            pos.z = bottomLeft.z + Random.Range(boundaryPadding + halfSize, widthHeight.z - boundaryPadding - halfSize);

            return pos;
        }
    }
}