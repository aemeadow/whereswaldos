﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    Rigidbody player;
    public int moveSpeed = 50;
    public int maxMoveSpeed = 100;

    public bool canJump = true;
    public int jumpStrength = 20;
    public float waitForWallJump = 2f;
    //How slow a player must be moving in order to regain their jump. Should be very small, because in this case they're "sticking" to a wall in order to get a wall jump.
    public float maxJumpResetMovement = 0.1f;



    PairInformation firstPerson;

    // Start is called before the first frame update
    void Start()
    {
        player = this.gameObject.GetComponent<Rigidbody>();


        Cursor.lockState = CursorLockMode.Confined;
    }

    private bool timerStarted = false;
    private float timer = 0f;

    public bool wallJump = false;

    // FixedUpdate is called once per physics update
    void FixedUpdate()
    {
        //If the player is moving too fast in any direction...
        float flatMagnitude = Vector2.SqrMagnitude(new Vector2(player.velocity.x, player.velocity.z));
        if (flatMagnitude > maxMoveSpeed)
        {
            //Slow them down
            Vector3 clampedSpeed = new Vector3((maxMoveSpeed / flatMagnitude) * player.velocity.x, player.velocity.y, (maxMoveSpeed / flatMagnitude) * player.velocity.z);
            player.velocity = clampedSpeed;

        }
        
        if (Input.GetButton("Horizontal"))
        {
            //Debug.Log("A/D held");
            player.AddRelativeForce(new Vector3(Input.GetAxis("Horizontal") * moveSpeed, 0, 0));
        }

        if (Input.GetButton("Vertical"))
        {
            //Debug.Log("W/S held");
            player.AddRelativeForce(new Vector3(0, 0, Input.GetAxis("Vertical") * moveSpeed));
        }

        //To jump, add an explosive force under them (at their position)
        if(Input.GetButton("Jump") && canJump)
        {
            if (wallJump)
            {
                Vector3 adjustedForceOrigin = player.transform.position;
                player.AddRelativeForce(new Vector3(-Input.GetAxis("Horizontal") * 1000, 0, -Input.GetAxis("Vertical") * 1000));
                player.AddExplosionForce((jumpStrength * 1.6f), adjustedForceOrigin , 10);
                canJump = false;
                wallJump = false;
            }
            else
            {
                player.AddExplosionForce(jumpStrength, player.transform.position, 10);
                canJump = false;
            }
        }

        //If the player is not moving, and if someone is holding down a key (Because then they're pressing into a wall)
        if ((player.velocity.magnitude) <= maxJumpResetMovement)
        {
            //Start a timer for a wall jump
            if (!timerStarted)
            {
                timerStarted = true;
                timer = 0f;
            }
            else
            {
                timer += Time.deltaTime;
            }

            if(timer > waitForWallJump){
                canJump = true;
                wallJump = true;
                timerStarted = false;
            }
        }
        else
        {
            timerStarted = false;
            wallJump = false;
            timer = 0f;
        }

        //Stop the player when they let go of a key and are on the ground so they can manuever easier on small platforms
        //If the player is not holding in a direction, and is not in the air...
        if (!Input.anyKey && canJump)
        {
            //Heavily increase their mass and drag so they stop very quickly
            player.GetComponent<Rigidbody>().mass = 50;
            player.GetComponent<Rigidbody>().drag = 5;
        }
        else
        {
            player.GetComponent<Rigidbody>().mass = 1;
            player.GetComponent<Rigidbody>().drag = 0.2f;
        }



        //On click, tell the camera to get the person in front of us (if there is one)
        if (Input.GetMouseButtonDown(1))
        {
            //Get thing in front from camera.
            PairInformation result = this.GetComponent<CamLook>().GetPerson();

            //If it's null, don't do anything.
            if(result == null)
            {
                Debug.Log("No person found");
            }
            //We hit a person
            else
            {
                //If nobody is stored...
                if(firstPerson == null)
                {
                    //We want to find the pair of this person.
                    firstPerson = result;

                    //UPDATE UI HERE


                }
                else //If we have a person stored.
                {
                    //If they have the same name, they're pairs.
                    if(firstPerson.name == result.name)
                    {
                        //So... update the timer?

                        //UPDATE UI HERE

                        //Empty the person we're looking for.
                        firstPerson = null;
                    }
                }
            }
        }


    }

    private void OnCollisionStay(Collision collision)
    {
        foreach (ContactPoint hitPos in collision.contacts)
        {
            if (hitPos.normal.y > 0)
            {
                canJump = true;
            }
        }
    }
}
