﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamLook : MonoBehaviour
{

    public Camera MainCamera;

    public int xSensitivity = 1;
    public int ySensitivity = 1;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Invert the y input
        float xInput = Input.GetAxis("Mouse X") * xSensitivity;
        //Invert the y input
        float yInput = -Input.GetAxis("Mouse Y") * ySensitivity;

        //Rotate the Camera up or down
        MainCamera.gameObject.transform.Rotate(yInput, 0, 0);
        //Rotate the player object left or right
        this.gameObject.transform.Rotate(0, xInput, 0);

        



    }


    public PairInformation GetPerson()
    {
        RaycastHit hit;
        //If the raycast hit
        if (Physics.Raycast(this.transform.position, this.transform.eulerAngles, out hit, 25f))
        {
            if (hit.transform.GetComponent<PairInformation>() != null)
            {
                return hit.transform.GetComponent<PairInformation>();
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

}
