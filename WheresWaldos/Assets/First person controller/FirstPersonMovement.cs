﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FirstPersonMovement : MonoBehaviour
{
    public float speed = 5;
    Vector2 velocity;

    public timerscript timer;


    //The person whose twin we're looking for 
    private PairInformation firstPerson;

    [Header("Things for the Follow Camera")]
    public GameObject followCam;
    public GameObject followCamImage;
    public TextMeshProUGUI followCamName;

    private void Start()
    {
        followCamImage.SetActive(false);
    }


    void FixedUpdate()
    {
        velocity.y = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        velocity.x = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(velocity.x, 0, velocity.y);
    }


    private void Update()
    {
        //On click, tell the camera to get the person in front of us (if there is one)
        if (Input.GetMouseButtonDown(0))
        {
            //Get thing in front from camera.
            PairInformation result = this.transform.GetChild(0).GetComponent<FirstPersonLook>().GetPerson();

            //If it's null, don't do anything.
            if (result == null)
            {
                Debug.Log("No person found");
            }
            //We hit a person
            else
            {
                Debug.Log("WE HIT A PERSON!");
                //If nobody is stored...
                if (firstPerson == null)
                {
                    //We want to find the pair of this person.
                    firstPerson = result;

                    //Turn on the camera Image 
                    followCamImage.SetActive(true);
                    followCamName.text = "Following " + result.name;

                    //Parent the camera
                    followCam.transform.parent = firstPerson.transform;
                    //Put it in front of their face
                    followCam.transform.localPosition = new Vector3(0, 1.4f, 1);
                    followCam.transform.localEulerAngles = new Vector3(0, 180, 0);
                    
                }
                else //If we have a person stored.
                {
                    //If they have the same name, they're pairs.
                    if (firstPerson.name == result.name)
                    {
                        //So... update the timer?
                        timer.AddTime();

                        //Turn off the camera image
                        followCamImage.SetActive(false);
                        followCamName.text = "";

                        //Add a point

                        //Unparent the camera
                        followCam.transform.parent = null;

                        //KILL THE TWINS
                        GameObject.Destroy(firstPerson.gameObject);
                        GameObject.Destroy(result.gameObject);

                        //Empty the person we're looking for.
                        firstPerson = null;
                    }
                }
            }
        }
    }
}
